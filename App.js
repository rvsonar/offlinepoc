import React, { Component } from "react";
import { createStore, applyMiddleware } from 'redux'
//import { Provider } from 'react-redux'
import { Platform, StyleSheet, Text, View, Button, FlatList, Alert } from "react-native";
import allReducers from './recuders'
import TicketContainer from './containers/TicketContainer'
import { Provider } from "react-redux"

import createSagaMiddleware from 'redux-saga'
import rootSaga from './sagas/rootSaga'
import service from './service'
const sagaMiddleware = createSagaMiddleware();

const store = createStore(allReducers, applyMiddleware(sagaMiddleware))
//const unsubscribe = store.subscribe(() => console.log(store.getState()))
export default class App extends Component {

  render() {
    return (
      <Provider store={store}>
        <TicketContainer/>
      </Provider>
    );
  }
}

sagaMiddleware.run(rootSaga);