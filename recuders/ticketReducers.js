import {FETCH_TICKETS, FETCH_SUCCESS, FETCH_FAILED} from '../actions/actionTypes'

const ticketReducers = (tickets = [], action) => {
    switch(action.type) {
        case FETCH_SUCCESS:
            return action.receivedTickets;
        case FETCH_FAILED:
            return [];
        default:
            return tickets;
    }
}

export default ticketReducers;