import { combineReducers } from 'redux'
import ticketReducers from './ticketReducers'

const allReducers = combineReducers ({
    ticketReducers,
})

export default allReducers;