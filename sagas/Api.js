const url = 'https://jsonplaceholder.typicode.com/users'
const axios = require('axios');

function* getPendingTicketsFromApi() {
    const response = yield axios.get(url);
    //console.log(response);
    const tickets = yield response.status === 200 ? response.data : []
    return tickets;
}

export const Api = {
    getPendingTicketsFromApi
};

