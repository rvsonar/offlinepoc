import {FETCH_TICKETS, FETCH_SUCCESS, FETCH_FAILED} from '../actions/actionTypes'
import { put, takeLatest, select } from 'redux-saga/effects'
import { Api } from './Api'


const getTickets = (state) => state.ticketReducers


function* fetchPendingTickets() {
    try {
        //Fetch data from cache
        const data = yield select(getTickets);
        console.log(`Length : ${data.length}`);
        if(data && data.length > 0)
        {
            console.log("Returned data from cache");
            yield put({type: FETCH_SUCCESS, receivedTickets: data});
        }
        else
        {
            console.log("Fetching data from API");
            //Fetch and return data from network API
            const receivedTickets = yield Api.getPendingTicketsFromApi();
            //console.log(receivedTickets)
            // Store data to cache
            yield put({type: FETCH_SUCCESS, receivedTickets: receivedTickets});
        }
        
        
    } catch(error) {
        yield put({type: FETCH_FAILED, error});
    }
}

export function* watchFetchTickets() {
    yield takeLatest(FETCH_TICKETS, fetchPendingTickets);
}