import {call, all } from 'redux-saga/effects'
import { watchFetchTickets } from './ticketSagas'

export default function* rootSaga() {
    yield call(watchFetchTickets);
}