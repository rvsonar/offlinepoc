import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, Button, FlatList, Alert } from "react-native";

class TicketComponent extends Component {

  GetItem(item) {
 
    Alert.alert(item.toString());
 
  }

  FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: "#607D8B",
        }}
      />
    );
  }

  render() {
    return (
      <View style={styles.MainContainer}>
        <Text style={styles.sectionTitle}>Tickets</Text>
        <Text/>
        <Button
          title="Fetch Tickets"
          onPress={()=>this.props.onFetchTickets('asc')}
        />
        <Text />
        <FlatList
          data={this.props.tickets}
          ItemSeparatorComponent={this.FlatListItemSeparator}
          renderItem={({ item }) => (
            <Text
              style={styles.item}
              onPress={this.GetItem.bind(this, item.name)}
            >
              {" "}
              {item.id}{" "}{item.name}
            </Text>
          )}
        ></FlatList>
      </View>
    );
  }
}

export default TicketComponent

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: '#ffffff',
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: '#ffffff',
  },
  MainContainer: {

    justifyContent: 'center',
    margin: 10,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: '#000000',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: '#000000',
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: '#000000',
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});
