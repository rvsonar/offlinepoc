import {FETCH_TICKETS, FETCH_SUCCESS, FETCH_FAILED} from './actionTypes'

export const fetchTicketsAction = (sort) => {
    return {
        type: FETCH_TICKETS,
        sort
    }
}

export const fetchSuccessAction = (receivedTickets) => {
    return {
        type: FETCH_SUCCESS,
        receivedTickets
    }
}

export const fetchFailedAction = (error) => {
    return {
        type: FETCH_FAILED,
        error
    }
}