import axios from 'axios';
import { Service } from 'axios-middleware';
import AuthMiddleware from './middlewares/AuthMiddleware';
import KeychainManager from './managers/KeychainManager'

// Create a new service instance
const service = new Service(axios);

// Then register your middleware instances.
service.register([
  new AuthMiddleware(new KeychainManager()),
]);

export default service;