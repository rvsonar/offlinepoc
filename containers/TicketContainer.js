import { Connect, connect } from 'react-redux'
import TicketComponent from '../components/TicketComponent'

import { fetchTicketsAction, fetchSuccessAction, fetchFailedAction} from '../actions'

const mapStateToProps = (state) => {
    return {
        tickets: state.ticketReducers,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchTickets : () => {
            dispatch(fetchTicketsAction());
        },
    }
}

const TicketContainer = connect(mapStateToProps, mapDispatchToProps)(TicketComponent);
export default TicketContainer;