export default class AuthMiddleware {
    constructor(KeychainManager) {
      this.keychainManager = KeychainManager;
    }
  
    onRequest(config) {
        console.log(`URL : ${config.url}`)
        console.log(`Adding auth token : ${this.keychainManager.getAuthToken()}`);
        return {
            ...config,
            headers: {
              token: this.keychainManager.getAuthToken(),
              ...config.headers
            }
          };
    }
  }